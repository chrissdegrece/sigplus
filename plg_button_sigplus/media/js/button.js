/**@license sigplus editor button
 * @author  Levente Hunyadi
 * @version $__VERSION__$
 * @remarks Copyright (C) 2011-2017 Levente Hunyadi
 * @remarks Licensed under GNU/GPLv3, see http://www.gnu.org/licenses/gpl-3.0.html
 * @see     http://hunyadi.info.hu/projects/sigplus
 **/

document.addEventListener('DOMContentLoaded', function () {
    if (!Element.prototype.matches) {
        Element.prototype.matches =
            Element.prototype.msMatchesSelector ||
            Element.prototype.webkitMatchesSelector;
    }

    /** Extracts the Joomla parameter name from a control. */
    function get_param_name(ctrl) {
        var name = ctrl.getAttribute('name');
        var matches = name.match(/^params\[(.*)\]$/);
        return matches ? matches[1] : name;
    }

    var form = document.getElementById('sigplus-settings-form');  // configuration settings form

    // selectors to match all user controls (only controls with the "name" attribute correspond to real plug-in settings, others are auxiliary controls)
    var checkboxselector = 'input[name][type=checkbox]';
    var radioselector = 'input[name][type=radio]';
    var textselector = 'input[name][type=text]';
    var listselector = 'select[name]';
    var ctrlselector = [checkboxselector, radioselector, textselector, listselector].join();

    // initialize parameter values to those set on content plug-in configuration page
    if (window.parent.sigplus) {  // variable that holds configuration settings as JSON object with parameter names as keys
        [].forEach.call(form.querySelectorAll(ctrlselector), function (ctrl) {  // enumerate form controls in order of appearance
            var name = get_param_name(ctrl);
            var value = window.parent.sigplus[name];
            if (value) {  // has a default value
                if (ctrl.matches(checkboxselector)) {  // checkbox control
                    ctrl.checked = !!value;
                } else if (ctrl.matches(radioselector) && ctrl.value === '' + value) {  // related radio button (with value to assign matching button value)
                    ctrl.checked = true;
                } else if (ctrl.matches([textselector, listselector].join())) {  // text and list controls
                    ctrl.value = value;
                }
            }
        });
    }

    // bind event to make parameter value appear in generated activation code
    [].forEach.call(form.querySelectorAll('li'), function (item) {
        // create marker control
        var updatebox = document.createElement('input');
        updatebox.setAttribute('type', 'checkbox');

        // check marker control when parameter value is to be edited
        [].forEach.call(item.querySelectorAll(ctrlselector), function (elem) {
            elem.addEventListener('focus', function () {
                updatebox.checked = true;
            });
        });

        // insert marker control before parameter name label
        item.insertBefore(updatebox, item.firstChild);  // inject as first element
    });

    // selects all user controls but omits checkboxes and radio buttons that are not checked
    var checkedselector = ':checked';
    var activectrlselector = [checkboxselector + checkedselector, radioselector + checkedselector, textselector, listselector].join();

    // process parameters when form is submitted
    document.getElementById('sigplus-settings-submit').addEventListener('click', function () {
        var params = [];  // activation code to insert
        [].forEach.call(form.querySelectorAll('li'), function (item) {
            var updatebox = item.querySelector('input[type=checkbox]');  // retrieve as first element
            var ctrl = item.querySelector(activectrlselector);  // first element with a form name (i.e. a control that corresponds to a real setting)
            if (ctrl && updatebox && updatebox.checked) {  // verify whether parameter value has changed
                var name = get_param_name(ctrl);
                var value = ctrl.value;
                if (value) {  // omit missing values
                    if (/color$/.test(name) || !/^(0|[1-9]\d*)$/.test(value)) {  // quote color codes but not integer values
                        value = '"' + value + '"';
                    }
                    params.push(name + '=' + value);
                }
            }
        });

        if (window.parent) {  // trigger insert event in parent window
            window.parent.sigplusOnInsertTag(params.length > 0 ? ' ' + params.join(' ') : '');
        }
    });
});
