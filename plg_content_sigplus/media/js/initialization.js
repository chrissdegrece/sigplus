/**
* @file
* @brief    sigplus Image Gallery Plus client-side initialization script
* @author   Levente Hunyadi
* @version  $__VERSION__$
* @remarks  Copyright (C) 2009-2017 Levente Hunyadi
* @remarks  Licensed under GNU/GPLv3, see http://www.gnu.org/licenses/gpl-3.0.html
* @see      http://hunyadi.info.hu/projects/sigplus
*/

/**
* Unwraps galleries protected by <noscript>, applies anchor and image attributes and removes helper elements.
* @param {string} id An HTML identifier.
*/
function __sigplusInitialize(id) {
	'use strict';

	// unwrap galleries from <noscript> elements
	[].forEach.call(document.querySelectorAll('#' + id + '.sigplus-gallery noscript'), function (/** @type {!Element} */ item) {
		var elem = document.createElement('div');
		elem.innerHTML = /** @type {string} */ (item['innerText']);  // <noscript> elements are not parsed when javascript is enabled
		var replacement = elem.firstChild === elem.lastChild ? elem.firstChild : elem;  // unwrap child from parent element with single child
		item.parentNode.replaceChild(replacement, item);
	});

	// apply anchor and image attributes
	[].forEach.call(document.querySelectorAll('#' + id + '.sigplus-gallery a'), function (/** @type {!HTMLAnchorElement} */ anchor) {
		var image = /** @type {HTMLImageElement} */ (anchor.querySelector('img'));
		if (image) {
			anchor.setAttribute('title', image.getAttribute('alt'));
		}

		// assign summary text (with HTML support)
		var parent = anchor.parentNode;
		if (parent) {  // skip anchors that are not sigplus gallery image anchors
			var summary = /** @type {HTMLElement} */ (parent.querySelector('.sigplus-summary'));
			if (summary) {
				anchor.setAttribute('title', summary['innerText']);
				var html = summary.innerHTML;
				if (html) {
					anchor.setAttribute('data-summary', html);
				}

				var targetanchor = summary.querySelector('a');
				if (targetanchor) {  // summary contains an anchor, which should be set as a preferred target for the image
					anchor.setAttribute('data-href', targetanchor.href);
					anchor.setAttribute('data-target', targetanchor.target);
				}

				summary.parentNode.removeChild(summary);
			}

			// assign download URL
			var downloadanchor = /** @type {HTMLAnchorElement} */ (parent.querySelector('.sigplus-download'));
			if (downloadanchor) {
				anchor.setAttribute('data-download', downloadanchor.href);
				downloadanchor.parentNode.removeChild(downloadanchor);
			}

			var title = /** @type {HTMLElement} */ (parent.querySelector('.sigplus-title'));
			if (title) {
				var html = title.innerHTML;
				if (html) {
					anchor.setAttribute('data-title', html);
				}
				title.parentNode.removeChild(title);
			}
		}
	});

	// apply click prevention to galleries without lightbox
	[].forEach.call(document.querySelectorAll('#' + id + '.sigplus-lightbox-none a.sigplus-image'), function (/** @type {!HTMLAnchorElement} */ anchor) {
		var link = anchor.getAttribute('data-href');
		if (link) {  // there is a preferred target for the image
			anchor.href = link;
			anchor.target = anchor.getAttribute('data-target');
			anchor.removeAttribute('data-href');
			anchor.removeAttribute('data-target');
		} else {
			anchor.addEventListener('click', function (event) {
				event.preventDefault();
			});
		}
	});
}

/**
* Apply caption text templates.
* @param {string} id An HTML identifier.
* @param {string} titletemplate A template for titles to use for making substitutions.
* @param {string} summarytemplate A template for summary text to use for making substitutions.
*/
function __sigplusCaption(id, titletemplate, summarytemplate) {
	'use strict';

	/**
	* @param {number} bytes
	* @return {string}
	*/
	function bytesToSize(bytes) {
		if (bytes == 0) {
			return '0 B';
		} else {
			var sizes = ['B', 'KB', 'MB', 'GB', 'TB'];
			var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1000)));
			return (bytes / Math.pow(1000, i)).toPrecision(3) + ' ' + sizes[i];
		}
	};

	var anchors = document.querySelectorAll('#' + id + ' a.sigplus-image');
	titletemplate = titletemplate || '{$text}';
	summarytemplate = summarytemplate || '{$text}';
	[].forEach.call(anchors, function (/** @type {!HTMLAnchorElement} */ anchor, index) {
		/**
		* @param {string} template
		* @param {string} text
		*/
		function _subs(template, text) {
			var filename = decodeURIComponent(anchor.getAttribute('data-image-file-name') || anchor.pathname || '');
			var replacement = {  // template replacement rules
				text: text || '',
				filename: filename.match(/([^\/]*)$/)[1],  // keep only file name component from path (including extension)
				filesize: bytesToSize(anchor.getAttribute('data-image-file-size')),
				current: index + 1,  // index is zero-based but user interface needs one-based counter
				total: anchors.length
			};

			return template.replace(/\\?\{\$([^{}]+)\}/g, function (match, name) {
				return replacement[name];
			});
		}

		/**
		* @param {!HTMLElement} elem
		* @param {string} attr
		* @param {string} template
		*/
		function _subsattr(elem, attr, template) {
			elem.setAttribute(attr, _subs(template, elem.getAttribute(attr)));
		}

		// apply template to summary text
		_subsattr(anchor, 'data-summary', summarytemplate);

		// apply template to "title" attribute of anchor element
		_subsattr(anchor, 'title', summarytemplate);

		// apply template to "alt" attribute of image element wrapped in anchor
		var image = /** @type {HTMLImageElement} */ (anchor.querySelector('img'));
		if (image) {
			_subsattr(image, 'alt', titletemplate);
		}
	});
}
