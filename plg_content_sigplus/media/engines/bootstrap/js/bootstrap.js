/**@license Bootstrap integration for sigplus
 * @author  Levente Hunyadi
 * @version $__VERSION__$
 * @remarks Copyright (C) 2017 Levente Hunyadi
 * @see     http://hunyadi.info.hu/projects/sigplus
 **/

window.sigplus = window.sigplus || {};
window.sigplus.bootstrap = window.sigplus.bootstrap || {};
window.sigplus.bootstrap.initialize = function (labels) {
    labels = labels || {};
    var $ = jQuery;

    // append Bootstrap dialog box HTML
    $("body").append(''
        +   '<div id="sigplus-bootstrap" class="modal fade hide" tabindex="-1" role="dialog" aria-hidden="true">'
        +       '<div class="modal-header">'
        +           '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>'
        +           '<h3 class="modal-title">sigplus</h3>'
        +       '</div>'
        +       '<div class="modal-body" style="max-height:none;"><img /></div>'
        +       '<div class="modal-footer">'
        +           '<button class="btn previous" aria-hidden="true">'+ (labels.previous || 'Previous') +'</button>'
        +           '<button class="btn next" aria-hidden="true">'+ (labels.next || 'Next') +'</button>'
        +           '<button class="btn btn-primary" data-dismiss="modal" aria-hidden="true">'+ (labels.close || 'Close') +'</button>'
        +       '</div>'
        +   '</div>'
    );
    var dialog = $("#sigplus-bootstrap");
    var image = dialog.find(".modal-body > img");
    var title = dialog.find(".modal-title");
    var btnPrevious = dialog.find(".btn.previous");
    var btnNext = dialog.find(".btn.next");
    var activeSet;  // the set of elements that can be navigated in the dialog
    var activeItem;  // the image currently displayed in the dialog, always a member of the element set

    function update(item) {
        activeItem = item;
        image.attr("src", activeItem.attr("href"));  // set image
        title.html(activeItem.find("img").attr("alt"));  // set image caption
    }
    function navigate(offset) {
        var index = activeSet.index(activeItem) + offset;
        var size = activeSet.size();
        index = (index + size) % size;  // normalize to interval [0;size)
        update(activeSet.eq(index));
    }

    btnPrevious.click(function () {
        navigate(-1);
    });
    btnNext.click(function () {
        navigate(1);
    });

    window.sigplus.bootstrap.show = function (set, item, options) {
        activeSet = set;
        update(item);
        btnPrevious.toggleClass('sigplus-hidden', set.size() < 2);
        btnNext.toggleClass('sigplus-hidden', set.size() < 2);
        dialog.modal(options);  // initialize and show modal box
    }
};
