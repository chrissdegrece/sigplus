# jbuild Custom build system for Joomla! 3.0
# Copyright 2009-2017 Levente Hunyadi <http://hunyadi.info.hu>
#
# jbuild is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# jbuild is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Requires Python 3.1 or later

import io
import os
import os.path
import platform
import shutil
import re
import subprocess
import tarfile
import time
import xml.dom.minidom
import zipfile

positive_extension_preference = ('.html','.svg','.css','.xml','.ini','.txt','.php','.sql','.js')  # prefer to be first
negative_extension_preference = ('.gif','.png')  # prefer to be last

def find_in_path(file, path=None):
    """find_in_path(file[, path=os.environ['PATH']]) -> list

    Finds all files with a specified name that exist in the operating system's
    search path (os.environ['PATH']), and returns them as a list in the same
    order as the path.  Instead of using the operating system's search path,
    the path argument can specify an alternative path, either as a list of paths
    of directories, or as a single string seperated by the character os.pathsep.

    If you want to limit the found files to those with particular properties,
    use filter() or which()."""

    if path is None:
        path = os.environ.get('PATH', '')
    if type(path) is type(''):
        path = path.split(os.pathsep)
    if platform.system() == 'Windows':
        path.extend(map(lambda d: os.path.join(os.environ.get('WINDIR'), d), ['System','System32','SysWoW64']))
    return filter(os.path.isfile, map(lambda d: os.path.join(d, file), path))

def which(file, mode=os.F_OK | os.X_OK, path=None):
    """which(file[, mode][, path=os.environ['PATH']]) -> list

    Finds all executable files in the operating system's search path
    (os.environ['PATH']), and returns them as a list in the same order as the
    path.  Like the UNIX shell command 'which'.  Instead of using the operating
    system's search path, the path argument can specify an alternative path,
    either as a list of paths of directories, or as a single string seperated by
    the character os.pathsep.

    Alternatively, mode can be changed to a different os.access mode to
    check for files or directories other than "executable files".  For example,
    you can additionally enforce that the file be readable by specifying
    mode = os.F_OK | os.X_OK | os.R_OK."""

    return filter(lambda path: os.access(path, mode), find_in_path(file, path))

def find_executable(executable):
    pathext = os.environ['PATHEXT'].lower().split(os.pathsep)
    (base, ext) = os.path.splitext(executable)
    if pathext and not ext:
        paths = (path for ext in pathext for path in find_in_path(executable + ext))
    else:
        paths = find_in_path(executable)
    try:
        return next(paths)
    except StopIteration:
        return None

def as_int(arg):
    """as_int(string) -> int

    Convert string to integer, returning None for input None."""

    if arg is None:
        return None
    else:
        return int(arg)

class Version:
    """Version information on a product in the format "x.y.z.w" where
    * x is the major version,
    * y is the minor version,
    * z is the revision number and
    * w is the incremental build number."""

    def __init__(self, majorversion, minorversion = None, maintenance = None, build = None):
        self.majorversion = majorversion
        self.minorversion = minorversion
        self.maintenance = maintenance
        self.build = build

    def as_list(self):
        version = [self.majorversion]
        if self.minorversion is not None:
            version.append(self.minorversion)
            if self.maintenance is not None:
                version.append(self.maintenance)
                if self.build is not None:
                    version.append(self.build)
        return [str(elem) for elem in version]

    def __str__(self):
        version = self.as_list()
        version[-1:] = []
        return '.'.join(version)

    @staticmethod
    def from_string(text):
        m = re.search(r'(\d+)(?:[.](\d+)(?:[.](\d+)(?:[.](\d+))?)?)?', text)
        if m:
            majorversion = as_int(m.group(1))
            minorversion = as_int(m.group(2))
            maintenance = as_int(m.group(3))
            build = as_int(m.group(4))
            return Version(majorversion, minorversion, maintenance, build)
        else:
            return None

    def as_string(self):
        return '.'.join(self.as_list())

    def as_time(self):
        if self.minorversion is None:
            minorversion = 0
        else:
            minorversion = self.minorversion
        if self.maintenance is None:
            maintenance = 0
        else:
            maintenance = self.maintenance
        t = time.localtime()
        t = (t.tm_year,t.tm_mon,t.tm_mday,self.majorversion,minorversion,maintenance,t.tm_wday,t.tm_yday,t.tm_isdst);
        return time.mktime(t)

class Packager:
    """Builds a distribution package of files in a folder."""

    def __init__(self, basepath, packagename, version, folder = None):
        # version information
        self.version = version
        # distribution archive
        self.basepath = basepath
        # last modification time for files
        self.mtime = version.as_time();

    def __del__(self):
        self.close()

    def close(self):
        self.archive.close()
        if hasattr(self, 'fileobj') and self.fileobj:
            self.fileobj.close()

    def get_file_contents(self, fullpath):
        """Opens a text file and replaces version placeholders."""

        with open(fullpath, 'r', encoding='utf-8') as f:
            t = f.read()
            t = t.replace('$__VERSION__$', str(self.version))
            t = t.replace('$__VERSION_NUMBER__$', self.version.as_string())
            t = t.replace('$__DATE__$', time.strftime("%b %d, %Y", time.gmtime()))
            return t.encode('utf-8')

    def is_text(self, fullpath):
        """Checks if version placeholders should be replaced in a file."""

        (basename,extension) = os.path.splitext(fullpath)
        return extension in ['.css','.html','.ini','.js','.php','.sql','.svg','.txt','.xml']

    @staticmethod
    def create(basepath, packagename, version, folder):
        if packagename.endswith(('.tar','.tgz','.tar.gz')):
            return TarPackager(basepath, packagename, version, folder)
        else:
            return ZipPackager(basepath, packagename, version, folder)

class TarPackager(Packager):
    def __init__(self, basepath, packagename, version, folder = None):
        Packager.__init__(self, basepath, packagename, version, folder)
        if packagename.endswith(('.tar.gz','.tgz')):
            mode = 'w:gz'
            packagefilename = packagename
        elif packagename.endswith('.tar'):
            mode = 'w'
            packagefilename = packagename
        else:
            mode = 'w:gz'
            packagefilename = packagename + '.tgz'
        if folder:
            self.fileobj = open(os.path.join(folder, packagefilename), 'wb')
        else:
            self.fileobj = None
        self.archive = tarfile.open(packagefilename, mode, format=tarfile.GNU_FORMAT, fileobj=self.fileobj)

    def add(self, relpath, fullpath = None):
        if not fullpath:
            fullpath = os.path.join(self.basepath, relpath)
        info = self.archive.gettarinfo(fullpath)
        info.name = relpath;
        info.mtime = self.mtime;
        if info.isfile():
            if self.is_text(fullpath):
                b = self.get_file_contents(fullpath)
                info.size = len(b)
                with io.BytesIO(b) as s:
                    self.archive.addfile(info, s)
            else:
                with open(fullpath, 'rb') as f:
                    self.archive.addfile(info, f)
        elif info.isdir():
            self.archive.addfile(info)

class ZipPackager(Packager):
    def __init__(self, basepath, packagename, version, folder = None):
        Packager.__init__(self, basepath, packagename, version, folder)
        if packagename.endswith(('.ar.zip','.store.zip')):
            self.compression = zipfile.ZIP_STORED  # no compression
            packagefilename = packagename
        elif packagename.endswith('.zip'):
            self.compression = zipfile.ZIP_DEFLATED
            packagefilename = packagename
        else:
            self.compression = zipfile.ZIP_DEFLATED
            packagefilename = packagename + '.zip'
        if folder:
            packagefilename = os.path.join(folder, packagefilename)
        self.archive = zipfile.ZipFile(packagefilename, 'w', self.compression)

    def add(self, relpath, fullpath = None):
        if not fullpath:
            fullpath = os.path.join(self.basepath, relpath)
        if not os.path.isdir(fullpath):
            info = zipfile.ZipInfo(relpath, time.gmtime(self.mtime)[0:6])
            info.compress_type = self.compression
            if self.is_text(fullpath):
                b = self.get_file_contents(fullpath)
                self.archive.writestr(info, b)
            else:
                with open(fullpath, 'rb') as f:
                    self.archive.writestr(info, f.read())

def package(basepath, packagename, version, folder = None, basepathover = None, languagefilter = None):
    """Package source files into a distribution with overrides."""

    def package_filter(filefilterfun = None, dirfilterfun = None):
        """Package files that meet filter criteria."""

        fullpaths = []
        for (path, folders, files) in os.walk(basepath):
            folders[:] = [folder for folder in folders if not folder.startswith('.')]  # skip hidden folders, e.g. ".svn"
            files[:] = [file for file in files if not file.startswith('.')]  # skip hidden file, e.g. ".gitignore"
            files[:] = filter(lambda file: not re.search(r'[.]override[.][^.]+$', file), files)  # filter ".override.ext" files
            fullpaths.extend([os.path.join(path, folder) for folder in filter(dirfilterfun, folders)])
            fullpaths.extend([os.path.join(path, file) for file in filter(filefilterfun, files)])
        return fullpaths

    packager = Packager.create(basepath, packagename, version, folder)
    def package_item(fullpath):
        relpath = os.path.relpath(fullpath, basepath)

        (path, name) = os.path.split(fullpath)
        (basename, extension) = os.path.splitext(name)
        overpath = os.path.join(path, basename + '.override' + extension)
        if os.path.exists(overpath):  # check for presence of an ".override.ext" file
            packager.add(relpath, overpath)
        elif basepathover:
            fullpathover = os.path.join(basepathover, relpath)
            if os.path.exists(fullpathover):
                packager.add(relpath, fullpathover)
            else:
                packager.add(relpath)
        else:
            packager.add(relpath)
        print(relpath + ' added')

    items = []
    def package_items(filefilterfun = None, dirfilterfun = None):
        for i in package_filter(filefilterfun, dirfilterfun):
            if i not in items:
                package_item(i)
                items.append(i)

    package_items(lambda entry: False, lambda entry: True)
    # process files with positive extension preference
    for extension in positive_extension_preference:
        package_items(lambda entry: entry.endswith(extension), lambda entry: False)
    # process files with other extensions
    package_items(lambda entry: not entry.endswith(positive_extension_preference) and not entry.endswith(negative_extension_preference), lambda entry: False)
    # process files with negative extension preference
    for extension in negative_extension_preference:
        package_items(lambda entry: entry.endswith(extension), lambda entry: False)

    packager.close()

def parse_xml(fullpath):
    def stripSpace(node):
        for child in node.childNodes:
            if child.nodeType == xml.dom.minidom.Node.TEXT_NODE:
                child.data = child.data.strip()
            else:
                stripSpace(child)
        if node.hasChildNodes():
            node.normalize()

    dom = None
    with open(fullpath, 'r', encoding='utf-8') as f:
        dom = xml.dom.minidom.parse(f)
    if dom:
        stripSpace(dom)
    return dom

def write_xml(fullpath, dom):
    with open(fullpath, 'w', encoding='utf-8') as f:
        xmltext = dom.toprettyxml(indent='\t', newl='\n', encoding='utf-8').decode('utf-8')
        xmlpretty = re.compile(r'>\s+(\S[^<>]*?)\s+</', re.DOTALL)  # strip whitespace surrounding text nodes
        f.write(xmlpretty.sub(r'>\g<1></', xmltext))

def update_languages(basepath, packagexml, filterfun = None):
    fullpath = os.path.join(basepath, packagexml)
    (basename,extension) = os.path.splitext(packagexml)
    overpath = os.path.join(basepath, basename + '.override' + extension)

    if os.path.exists(overpath):
        dom = parse_xml(overpath)
    else:
        dom = parse_xml(fullpath)

    def get_node_by_tag_name(tag):
        nodes = dom.getElementsByTagName(tag)
        if nodes:
            return nodes[0]
        else:
            return None

    def remove_children(node):
        for child in [child for child in node.childNodes]:  # force building a list of nodes
            node.removeChild(child)
            child.unlink()

    if dom:
        languagesnode = get_node_by_tag_name('languages')
        if languagesnode:
            remove_children(languagesnode)
            for (language,country,name) in list_languages(os.path.join(basepath, languagesnode.getAttribute('folder')), filterfun):
                languagenode = dom.createElement('language')
                languagenode.setAttribute('tag', language + '-' + country)
                languagenode.appendChild(dom.createTextNode(language + '-' + country + '.' + name + '.ini'))
                languagesnode.appendChild(languagenode)

        write_xml(overpath, dom)

def get_version(basepath, packagexml, update=False):
    """Fetch and update version number."""

    t = False
    fullpath = os.path.join(basepath, packagexml)
    with open(fullpath, 'r', encoding='utf-8') as f:
        t = f.read()
        m = re.search(r'<version>(\d+)(?:[.](\d+)(?:[.](\d+)(?:[.](\d+))?)?)?</version>', t)
        if m:
            majorversion = as_int(m.group(1))
            minorversion = as_int(m.group(2))
            maintenance = as_int(m.group(3))
            build = as_int(m.group(4))
            if not build:
                build = 0
            if update:
                build += 1
            version = Version(majorversion, minorversion, maintenance, build)
            t = t.replace(m.group(0), '<version>' + version.as_string() + '</version>')
    if update and t:
        with open(fullpath, 'w', encoding='utf-8') as f:
            f.write(t)
    return version

def get_version_xml(basepath, packagexml, update=False):
    """Fetch and update version number."""

    fullpath = os.path.join(basepath, packagexml)
    dom = parse_xml(fullpath)

    def get_node_by_tag_name(tag):
        nodes = dom.getElementsByTagName(tag)
        if nodes:
            return nodes[0]
        else:
            return None

    def get_inner_text(node):
        if node.childNodes.length == 1 and node.childNodes[0].nodeType == xml.dom.minidom.Node.TEXT_NODE:
            return node.childNodes[0].data
        else:
            return None

    def set_inner_text(node, text):
        if node.childNodes.length == 1 and node.childNodes[0].nodeType == xml.dom.minidom.Node.TEXT_NODE:
            child = node.childNodes[0]
            node.replaceChild(dom.createTextNode(text), child)
            child.unlink()

    if dom:
        versionnode = get_node_by_tag_name('version')
        if versionnode:
            version = Version.from_string(get_inner_text(versionnode))
            if update:
                version.build += 1
                set_inner_text(versionnode, version.as_string())

        if update:
            (basepath,extension) = os.path.splitext(fullpath);
            #write_xml(basepath + '.override' + extension, dom)

        return version
    else:
        return None

def minify_yui(uncompressed, compressed, method):
    sinfo = subprocess.STARTUPINFO()
    # sinfo.dwFlags |= subprocess.STARTF_USESHOWWINDOW
    subprocess.check_call([find_executable('java'),'-jar',os.path.join(os.path.split(__file__)[0],'yuicompressor.jar'),'-v','--charset','utf8','-o',compressed,uncompressed], startupinfo=sinfo)

def minify_google(uncompressed, compressed, method):
    sinfo = subprocess.STARTUPINFO()
    # sinfo.dwFlags |= subprocess.STARTF_USESHOWWINDOW
    params = ['--language_in','ECMASCRIPT6_STRICT','--language_out','ECMASCRIPT5_STRICT']
    if method == 'advanced':
        params.extend(['--compilation_level','ADVANCED_OPTIMIZATIONS','--output_wrapper','(function(){%output%})();','--warning_level','VERBOSE','--jscomp_warning=reportUnknownTypes'])
    else:
        params.extend(['--compilation_level','SIMPLE_OPTIMIZATIONS'])
    subprocess.check_call([find_executable('java'),'-jar',os.path.join(os.path.split(__file__)[0],'closure-compiler.jar'),'--js',uncompressed,'--js_output_file',compressed,'--charset','UTF-8'] + params, startupinfo=sinfo)

def minify_microsoft(uncompressed, compressed, method):
    sinfo = subprocess.STARTUPINFO()
    # sinfo.dwFlags |= subprocess.STARTF_USESHOWWINDOW
    subprocess.check_call([os.path.join(os.path.split(__file__)[0],'AjaxMin.exe'),'-analyze','-clobber:true','-literals:combine',uncompressed,'-out',compressed], startupinfo=sinfo)

def minify_file(path, f, minify_method = minify_yui, compression_method = 'simple'):
    (basename,extension) = os.path.splitext(f)
    uncompressed = os.path.join(path, f)
    compressed = os.path.join(path, basename + '.min' + extension)
    if os.path.exists(compressed):
        if os.path.getmtime(uncompressed) > os.path.getmtime(compressed):
            print(f + ' has changed')
            minify_method(uncompressed, compressed, compression_method)
        else:
            print(f + ' is up-to-date')

def minify(basepath, method = 'simple'):
    """Minify javascript files."""
    for (path, folders, files) in os.walk(basepath):
        for f in files:
            (basename,extension) = os.path.splitext(f);
            if extension == '.js':
                minify_file(path, f, minify_google, method)
            elif extension == '.css':
                minify_file(path, f, minify_yui, method)

def list_languages(basepath, filterfun = None):
    """List language localization files in a directory."""
    languagelist = []
    for entryname in os.listdir(basepath):
        m = re.match(r'^([a-z]{2})-([A-Z]{2})\.(.*)\.ini$', entryname)
        if m:
            language = m.group(1)
            country = m.group(2)
            name = m.group(3)
            languagelist.append( (language,country,name) )
    return filter(filterfun, languagelist)

if __name__ == '__main__':
    print('Custom build system for Joomla! 3.0\nCopyright 2009-2017 Levente Hunyadi <http://hunyadi.info.hu>')
    print('This file is meant to be used as a Python module and not run directly\nfrom the command line.')
