@echo off
rem jbuild Custom build system for Joomla! 1.5 & 1.6
rem Copyright 2009-2011 Levente Hunyadi <http://hunyadi.info.hu>

title jbuild Custom build system for Joomla

rem Check for mandatory arguments
if "%~1"=="" goto argmissing

rem Traverse common locations for program files
for /d %%p in ("%ProgramFiles%","%ProgramW6432%","%ProgramFiles(x86)%") do (
	rem Look for Python interpreter executable
	for /d %%d in ("%%~p\python*") do (
		if exist "%%~d\python.exe" (
			rem Save current environment
			setlocal

			rem Add script file directory to Python library path
			set PYTHONPATH=%~dp0

			rem Invoke Python interpreter passing arguments
			"%%~d\python.exe" -O %*

			rem Restore environment
			endlocal
			goto quit
		)
	)
)

rem Display error message
:nointerpreter
echo Python interpreter not found.
goto quit

:argmissing
echo Syntax: callpy buildscript.py
goto quit

rem Quit normally
:quit