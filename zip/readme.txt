
sigplus: an image gallery distribution for Joomla

Copyright 2009-2017 Levente Hunyadi
http://hunyadi.info.hu/sigplus


DESCRIPTION

sigplus adds photo and multimedia galleries to a Joomla article with
a simple syntax. It takes a matter of minutes to set up but comes with
a vast range of options for more advanced users.

Even while simple to use, sigplus offers an impressive set of features.
Images suitable for gallery preview are automatically generated with
cropping and/or centering, and clicking on a preview image brings up a
pop-up window overlay (so-called lightbox) showing the original image
without navigating away from the current page. Images in a gallery can be
displayed in fixed or flow layout, they can be shown on a slider/rotator.
Appearance and behavior are highly configurable including image margin,
border, padding, slider/rotator orientation (horizontal or vertical),
animation speed, slideshow delay, image sort order, etc.

Despite its versatility, sigplus is completely free. It is a full-fledged
product with all features included, there is no need to buy a commercial
professional version for extra features or purchase club membership to
unlock additional privileges. Since its publication in the Joomla
Extensions Directory in March 2010, sigplus has been rated the best
extension in the "Photo gallery" category.


FEATURES

* user-selectable pop-up window engine, including boxplus (specifically
designed for sigplus), Bootstrap Modal, Fancybox, Fancybox3 and Slimbox2

* user-selectable image slider/carousel/rotator

* free-flow and grid layout mode; row, column and grid arrangement

* fully responsive, mobile-enabled

* support for image types .jpg, .png and .gif (with and without
transparency, with and without animation)

* multimedia support (HTML <video> tag)

* best-fit thumbnail generation with automatic cropping and centering

* progressive load feature to save network bandwidth: only those images
are fetched from the server that are to be shown

* multiple galleries per content item and per page

* multilingual, search engine-friendly image labels and more verbose
description set globally or for each individual image

* right-to-left language support

* image caption templates

* image metadata processing; IPTC and EXIF data extraction

* download option to save high-resolution image version

* custom styling (e.g. preview image margin, border, padding and opacity,
slide duration and animation delay)

* custom sort criteria (user-defined, file name, last modification time
and random order)

* integrates into Joomla content editor and Joomla search

* large gallery support (100 or more images in a single gallery)

* 100% CSS3 and JavaScript, HTML5-enabled

* default global settings for the entire site and local parameter
overrides for individual galleries

* restricted-access galleries

* and more...


REQUIREMENTS

* Joomla 3.0 or later
* GD2 (Graphics Draw) and/or ImageMagick library
* compatible with both MySQL and Microsoft SQL Server
